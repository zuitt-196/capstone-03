//[SECTION] DEPENDENCIES
const jwt = require('jsonwebtoken');
const secret = "CourseBookingAPI";

module.exports.createAccessToken = (user) => {
	console.log(user);

	// data object is created to contain some details of user
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	console.log(data);

	return jwt.sign(data, secret, {})
}

module.exports.verify = (req, res, next) => {

	// req.headers.authorization- contains jwt/ token
	let token = req.headers.authorization;

	if(typeof token === "undefined"){
		// typeof result is a string

		return res.send({auth: "Failed. No Token"})
	
	} else {
		console.log(token);
		token = token.slice(7, token.length)
		console.log(token)

		jwt.verify(token, secret, (err, decodedToken) => {

			if(err){
				return res.send({
					auth: "Failed",
					message: err.message
				})
			
			} else {
				console.log(decodedToken);

				req.user = decodedToken

				// will let us proceed to the next middleware or controller
				next();
			}
		})
	}
};

// verifying an admin

module.exports.verifyAdmin = (req, res, next) => {

	if(req.user.isAdmin){

		next();
	
	} else {

		return res.send({
			auth: "Failed",
			message: "Action Forbidden"
		})
	}
};