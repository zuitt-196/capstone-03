const mongoose = require("mongoose");
const orderSchema = new mongoose.Schema({

	totalAmount: {
		type: Number,
		required: [true,"Total amount is required"]
	},
	purchaseOn: {
		type: Date,
		default: new Date()
	},
	userId: {
		type: String
	},
	products:[
			{
				productId: {
					type: String,
					required: [true,"Product id is required"]
				},
				quantity: {
					type: Number,
					required: [true,"Quantity is required"]
				}
			}

			]
	})

module.exports = mongoose.model("Order",orderSchema);

