const User = require("../models/User");
const Product = require("../models/Product")
const Order = require("../models/Order")

const bcrypt = require("bcrypt");
const auth = require("../auth");


// register User
module.exports.registerUser = (req,res) =>{
    const hashedPw = bcrypt.hashSync(req.body.password,10);
    let newUser = new User({
        
        firstName:req.body.firstName,
        lastName:req.body.lastName,
        email:req.body.email,
        password:hashedPw,
        mobileNo:req.body.mobileNo
    })
    newUser.save()
    .then(result=>res.send(result))
    .catch(err=>res.send(err))
}

//log in getting token
module.exports.loginUser = (req,res) =>{
    User.findOne({email:req.body.email})
    .then(foundUser=>{
        if(foundUser === null){
            return res.send({message:"No User Found."});
        } else {
            const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);
            if(isPasswordCorrect){
                return res.send({accessToken:auth.createAccessToken(foundUser)});
            } else {
                return res.send({message:"Username and/or password is incorrect."});
            }
        }
    })
    .catch(err=>res.send(err))
}

// Get User Details
module.exports.getUserDetails = (req,res) => {
    User.findById(req.user.id)
    .then(result => res.send(result))
    .catch(error => res.send(error))
}


// update user to become Admin
module.exports.updateAdmin = (req,res) =>{
    let update = {
        isAdmin:true
    }
    User.findByIdAndUpdate(req.params.id,update,{new:true})
    .then(result=>res.send(result))
    .catch(err=>res.send(err));
}

