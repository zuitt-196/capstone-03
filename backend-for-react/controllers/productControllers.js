
const Product = require("../models/Product");
const Order = require("../models/Order");

// Adding Product
module.exports.addProduct = (req,res) =>{
    let newProduct = new Product({
        name:req.body.name,
        description:req.body.description,
        price:req.body.price
    })
    newProduct.save()
    .then(result=>res.send(result))
    .catch(err=>res.send(err))
}

// Getting Active Product
module.exports.getActiveProducts = (req,res) =>{
    Product.find({isActive:true})
    .then(result=>res.send(result))
    .catch(err=>res.send(err))
}


module.exports.retrieveProduct = (req,res) =>{
    Product.findById(req.params.id)
    .then(result=>res.send(result))
    .catch(err=>res.send(err))
}


module.exports.updateProduct = (req,res) =>{
    let update = {
        name:req.body.name,
        description:req.body.description,
        price:req.body.price
    }
    Product.findByIdAndUpdate(req.params.id,update,{new:true})
    .then(result=>res.send(result))
    .catch(err=>res.send(err))
}

module.exports.archiveProduct = (req,res) =>{
    let update = {
        isActive:false
    }
    Product.findOneAndUpdate(req.params.id,update,{new:true})
    .then(result=>res.send(result))
    .catch(err=>res.send(err))
}

