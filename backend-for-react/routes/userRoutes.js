const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");

const auth = require("../auth");
const {verify,verifyAdmin} = auth;

// register User
router.post('/register',userControllers.registerUser);

// getting token
router.post('/login',userControllers.loginUser);

// get user details
router.get('/userDetails',verify,userControllers.getUserDetails);

//  update user to become Admin
router.put('/updateAdmin/:userId',verify,verifyAdmin,userControllers.updateAdmin);

module.exports = router;

