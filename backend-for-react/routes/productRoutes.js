 
const express = require("express");
const router = express.Router();

const auth = require("../auth")
const {verify, verifyAdmin} = auth;

const productControllers = require("../controllers/productControllers");

router.post('/',verify,verifyAdmin,productControllers.addProduct);

router.get('/active',productControllers.getActiveProducts);

router.get('/getSingleProduct/:id',productControllers.retrieveProduct)

router.put('/updateProduct/:id',verify,verifyAdmin,productControllers.updateProduct);

router.delete('/archiveProduct/:id',verify,verifyAdmin,productControllers.archiveProduct);

module.exports = router;

