

import {Link} from 'react-router-dom';
// import {Navbar, Container, Nav} from 'react-bootstrap';


import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';



export default function AppNavbar(){


	// state hook to store the user information stored in the login page (without provider)
	// const [user, setUser] = useState(localStorage.getItem("email"));
	// console.log(user);

	return (
		<Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
		<Container>
		<Navbar.Brand as={Link} to="/">PEOPLES HARDWARE SHOP</Navbar.Brand>
		  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
		  <Navbar.Collapse id="responsive-navbar-nav">
			<Nav className="me-auto">
				<Nav.Link as={Link} to="/">Home</Nav.Link>
				<Nav.Link as={Link} to="/product">Products</Nav.Link>
			</Nav>
			<Nav>
				

					<Nav.Link as={Link} to="/login">Login</Nav.Link>
	           		<Nav.Link as={Link} to="/register">Register</Nav.Link>
	      
	     	

			</Nav>
		  </Navbar.Collapse>
		</Container>
	  </Navbar>

		

	)	
};













{/* <Navbar bg="light" expand="lg" className='flex mt-5'>
	  <Container>
	    <Navbar.Brand as={Link} to="/">Andress Soriano Colleges of Bislig </Navbar.Brand>
	     <Navbar.Toggle aria-controls="basic-navbar-nav" />
	       <Navbar.Collapse id="basic-navbar-nav">
	        <Nav className="me-auto">         
			   <Nav.Link as={Link} to="/">Home</Nav.Link>
	           <Nav.Link as={Link} to="/courses">Courses</Nav.Link>
			
			{
				(user.id !== null) ?
					<Nav.Link as={Link} to="/logout">Logout</Nav.Link>       
				:
				<>
					<Nav.Link as={Link} to="/login">Login</Nav.Link>
	           		<Nav.Link as={Link} to="/register">Register</Nav.Link>
	           	</>
	     	}

	         </Nav>
	       </Navbar.Collapse>
	   </Container>
	</Navbar> */}








