import {useState} from 'react';
import {Card, Button, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function CourseCard(props){
	const {breakpoint, productProp} = props

	const {name, description, price, _id} = productProp;

return(
	<Col xs={12} md={breakpoint} className="mt-4">
	<Card className="card1 p-3 mb-3">
		<Card.Body>
			<Card.Title className="fw-bold">{name}</Card.Title>
			<Card.Subtitle> Product Description: </Card.Subtitle>
			<Card.Text>
				{description}
			</Card.Text>
			<Card.Subtitle> Product Price: </Card.Subtitle>
			<Card.Text>{price}</Card.Text>
			<Link className="btn btn-primary" to={`/productView/${_id}`}>View Details</Link>
		</Card.Body>
	</Card>
	</Col>

)
}
