import {useState, useEffect} from 'react';
import {Row} from 'react-bootstrap'
// import coursesData from '../data/coursesData';
import ProductCard from '../components/ProductCard';

export default function Products(){
	
	const [products, setProduct] = useState([]);

		useEffect(() => {
		fetch('http://localhost:4000/products')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			const productArr = (data.map(products => {

				return (
					<ProductCard key={products._id} productProp={products} breakpoint={4} />
				)
				
			}))
			setProduct(productArr)
		})

	}, [products])


	return(

		<>
		<Row>
			<h1>Available Products:</h1>
			{products}
		</Row>
		</>
	)
}





