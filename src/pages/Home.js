
import Highlights from '../components/Highlights';

export default function Home() {
	const data = {
        title: "Andress Soriano Colleges of Bislig",
        content: "Enroll the courses that we offer",
        destination: "/courses",
        label: "Enroll now!"
    }

    return (
        <>
	
	        <Highlights />
		</>

    )
}

