
import {useState,} from 'react';
import { Form, Button } from 'react-bootstrap';

export default function Login() {

	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    // const [isActive, setIsActive] = useState(false);

    return (
    	<>
    	<h1>Login Here:</h1>
        <Form>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    required
                    value={email}
	          		onChange={(e) => setEmail(e.target.value)}

                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Enter password" 
                    required
             		value={password}
	          		onChange={(e) => setPassword(e.target.value)}
                />
            </Form.Group>

                <Button variant="success" type="submit" id="submitBtn" className="mt-3 mb-5">
                    Login
                </Button>
              

            </Form>

        </>
    )
}

