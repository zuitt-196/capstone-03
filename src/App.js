
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Login from './pages/Login';
import Product from './pages/Product';
import './App.css';


function App() {
	return (

		<>	
	
				<Router>
					<AppNavbar/>
					<Container>
						<Routes>
							<Route exact path="/" element={<Home/>}/>
							<Route exact path="/login" element={<Login/>}/>
							<Route exact path="/product" element={<Product/>}/>
					
						</Routes>
					</Container>
				</Router>
			
		</>
	)
}

export default App;
